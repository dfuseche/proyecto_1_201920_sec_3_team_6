package test.logic;

import static org.junit.Assert.*;

import model.data_structures.Cola;
import model.logic.MVCModelo;

import org.junit.Before;
import org.junit.Test;

public class TestMVCModelo {
	
	private MVCModelo modelo;
	private static int CAPACIDAD=100;
	
	@Before
	public void setUp1() {
		modelo= new MVCModelo();
	}

	public void setUp2() throws Exception {
		modelo.cargar(2);
	}

	@Test
	public void testMVCModelo() {
		assertTrue(modelo!=null);
		assertEquals(0, modelo.totalViajesHora());
		assertEquals(0, modelo.totalViajesMes());
		assertEquals(0, modelo.totalViajesSemana());

	}

	

	public void testPromedioDeViajePorMes(double pMes)
	{
		setUp1();
		double p = modelo.promedioDeViajePorMes(2);
		assertTrue(p == 0);
		
		
	}
	
	public void testPromedioDeViajePorDia(double pMes)
	{
		setUp1();
		double p = modelo.promedioDeViajePorDia(2);
		assertTrue(p == 0);
		
		
	}

	public void testDarMayoresPromedioPorMes() throws Exception
	{
		setUp2();
		String c = modelo.darMayoresPromedio(2);
		assertTrue(c != "");
		
	}
	
	public void testDarMayoresPromedioPorDia() throws Exception
	{
		setUp2();
		Cola c = modelo.darMayoresPromedioPorDia(15);
		assertTrue(c.size() != 0);
		
	}

	public void testCompararTiemposPromedioPorViaje() throws Exception
	{
		setUp2();
		Cola c = modelo.compararTiemposPromedioPorViaje(8, 10);
		assertTrue(c.size() != 0);
	}
	
	public void testCompararTiemposPromedioPorViajePorDia() throws Exception
	{
		setUp2();
		Cola c = modelo.compararTiemposPromedioPorViajePorDia(1, 5);
		assertTrue(c.size() != 0);
	}
	
	public void testConsultarLosViajesEnFranjaHoraria(  ) throws Exception
	{
		setUp2();
		Cola c = modelo.consultarLosViajesEnFranjaHoraria(10, 12);
		assertTrue(c.size() != 0);
	}
	
	public void testDarMayorPromedioPorHora() throws Exception
	{
		setUp2();
		Cola c = modelo.darMayorPromedioPorHora(12);
		assertTrue(c.size() != 0);
	}
	
	public void tablaASCII() throws Exception
	{
		setUp2();
		String s = modelo.tablaASCII(12, 16);
		assertTrue(s != "");
	}

}
