package controller;

import java.io.IOException;
import java.util.Scanner;

import model.data_structures.Cola;

import model.logic.MVCModelo;
import model.logic.Uber;
import view.MVCView;

public class Controller {

	/* Instancia del Modelo*/
	private MVCModelo modelo;
	
	/* Instancia de la Vista*/
	private MVCView view;
	
	

	/**
	 * Crear la vista y el modelo del proyecto
	 * @param capacidad tamaNo inicial del arreglo
	 */
	public Controller ()
	{
		view = new MVCView();
		modelo = new MVCModelo();
	}
		
	public void run() 
	{
		Scanner lector = new Scanner(System.in);
		boolean fin = false;
		String dato = "";
		int tri = 0;
		String respuesta = "";

		while( !fin ){
			view.printMenu();

			int option = lector.nextInt();
			switch(option){
				case 1:
					System.out.println("--------- \nCargar: ");
					System.out.println("Digite el trimestre a consultar. 1 - 4");
					tri = lector.nextInt();
					try {
						modelo.cargar(tri);
						System.out.println(modelo.totalViajesHora());
						System.out.println(modelo.totalViajesSemana());
						System.out.println(modelo.totalViajesMes());
						System.out.println(modelo.totalViajesHora());
						
					} catch (Exception e) {
						e.printStackTrace();
					}					
					break;

				case 2:
					System.out.println("Digite el mes:");
					double mes = lector.nextDouble();
					System.out.println("Digite la zona de origen:");
					double ZonaO = lector.nextDouble();
					System.out.println("Digite la zona de destino: ");
					double ZonaF = lector.nextDouble();
					System.out.println("Hola");
					Cola r = modelo.promedioDeViajePorMes(mes, ZonaO, ZonaF);
					System.out.println("Metodo f");
					if(r.getTail() != null)
					{
						while( r.getTail() != null )
						{
							Uber actual = (Uber) r.getTail().getElement();
							System.out.println("Tiempo promedio de viaje: " + actual.getMeanTravel_time());
							System.out.println("Desviaci�n estandar: " + actual.getStandardDeviationTravelTime());
							
							r.dequeue();
						}
						
					}
					else
					{
						System.out.println("No se encontraron viajes entre la zona digitada");
					}
					
					
					break;

				case 3:
					System.out.println("Digite la cantidad de viajes a consultar (N)");
					int N = lector.nextInt();
					System.out.println("Digite el mes a consultar: ");
					double mes2 = lector.nextDouble();
					Uber[] respuesta2 = modelo.darMayoresPromedio(mes2, N);
					
					for(int i= 0; i < N; i++)
					{
						Uber actual = respuesta2[i];
						
						System.out.println("Zona de Origen: " + actual.getSourceId());
						System.out.println("Zona de Destino: " + actual.getDstid());
						System.out.println("Tiempo promedio: " + actual.getMeanTravel_time());
						System.out.println("Desviaci�n estandar: " + actual.getHour());
						
					}
					break;
				case 4:
					System.out.println("Digite la zona mayor");
					int zonaMayor = lector.nextInt();
					System.out.println("Digite la zona menor");
					int zonaMenor = lector.nextInt();
					System.out.println("Digite la zona X");
					int zonaX = lector.nextInt();
					System.out.println("Digite el mes");
					double M = lector.nextDouble();
					
					Cola r1 = modelo.compararTiemposPromedioPorViaje(zonaMayor, zonaMenor, zonaX, M);
					Cola r2 = modelo.darColaAyuda();
					
					while(r1.getTail() != null && r2.getTail() != null)
					{
						Uber actual = (Uber) r1.getTail().getElement();
						Uber actual2 = (Uber) r2.getTail().getElement();
						
						if(actual != null && actual2 != null)
						{
						System.out.println("<" + actual.getMeanTravel_time() +"> de <" +zonaMenor+"> a <"+ zonaX+"> vs <"+ actual2.getMeanTravel_time()+"> de <"+zonaX+"> a <"+zonaMayor+">" );
						}
						else if(actual == null && actual2 != null)
						{
							System.out.println("< No hay viajes > de <" +zonaMenor+"> a <"+ zonaX+"> vs <"+ actual2.getMeanTravel_time()+"> de <"+zonaX+"> a <"+zonaMayor+">" );	
						}
						else if(actual != null && actual2 == null)
						{
							System.out.println("<" + actual.getMeanTravel_time() +"> de <" +zonaMenor+"> a <"+ zonaX+"> vs < No hay viajes> de <"+zonaX+"> a <"+zonaMayor+">" );
						}
						else
						{
							System.out.println("<No hay viajes> de <" +zonaMenor+"> a <"+ zonaX+"> vs < No hay viajes> de <"+zonaX+"> a <"+zonaMayor+">" );

						}
						r1.dequeue();
						r2.dequeue();
						
					}
					break;
				case 5:
					System.out.println("Digite el d�a:");
					double dia = lector.nextDouble();
					System.out.println("Digite la zona de origen:");
					double ZonaOr = lector.nextDouble();
					System.out.println("Digite la zona de destino: ");
					double ZonaFi = lector.nextDouble();
					Cola res = modelo.promedioDeViajePorDia(dia, ZonaOr, ZonaFi);
					if(res.getTail() != null)
					{
						while( res.getTail() != null )
						{
							Uber actual = (Uber) res.getTail().getElement();
							System.out.println("Tiempo promedio de viaje: " + actual.getMeanTravel_time());
							System.out.println("Desviaci�n estandar: " + actual.getStandardDeviationTravelTime());
							
							res.dequeue();
						}
						
					}
					else
					{
						System.out.println("No se encontraron viajes entre la zona digitada");
					}
					
					
					
					break;
					
				case 6:
					System.out.println("Digite la cantidad de viajes a consultar (N)");
					int Numero = lector.nextInt();
					System.out.println("Digite el mes a consultar: ");
					double dia2 = lector.nextDouble();
					Uber[] rspta2 = modelo.darMayoresPromedio(dia2, Numero);
					
					for(int i= 0; i < rspta2.length; i++)
					{
						Uber actual = rspta2[i];
						
						System.out.println("Zona de Origen: " + actual.getSourceId());
						System.out.println("Zona de Destino: " + actual.getDstid());
						System.out.println("Tiempo promedio: " + actual.getMeanTravel_time());
						System.out.println("Desviaci�n estandar: " + actual.getHour());
						
					}
					break;
				
				case 7:
					System.out.println("Digite la zona mayor");
					int zonaMayorD = lector.nextInt();
					System.out.println("Digite la zona menor");
					int zonaMenorD = lector.nextInt();
					System.out.println("Digite la zona X");
					int zonaXD = lector.nextInt();
					System.out.println("Digite el d�a");
					double MD = lector.nextDouble();
					
					Cola r1D = modelo.compararTiemposPromedioPorViajePorDia(zonaMayorD, zonaMenorD, zonaXD, MD);
					Cola r2D = modelo.darColaAyuda();
					
					while(r1D.getTail() != null && r2D.getTail() != null)
					{
						Uber actual = (Uber) r1D.getTail().getElement();
						Uber actual2 = (Uber) r2D.getTail().getElement();
						
						if(actual != null && actual2 != null)
						{
						System.out.println("<" + actual.getMeanTravel_time() +"> de <" +zonaMenorD+"> a <"+ zonaXD+"> vs <"+ actual2.getMeanTravel_time()+"> de <"+zonaXD+"> a <"+zonaMayorD+">" );
						}
						else if(actual == null && actual2 != null)
						{
							System.out.println("< No hay viajes > de <" +zonaMenorD+"> a <"+ zonaXD+"> vs <"+ actual2.getMeanTravel_time()+"> de <"+zonaXD+"> a <"+zonaMayorD+">" );	
						}
						else if(actual != null && actual2 == null)
						{
							System.out.println("<" + actual.getMeanTravel_time() +"> de <" +zonaMenorD+"> a <"+ zonaXD+"> vs < No hay viajes> de <"+zonaXD+"> a <"+zonaMayorD+">" );
						}
						else
						{
							System.out.println("<No hay viajes> de <" +zonaMenorD+"> a <"+ zonaXD+"> vs < No hay viajes> de <"+zonaXD+"> a <"+zonaMayorD+">" );

						}
						r1D.dequeue();
						r2D.dequeue();
						
					}
					break;
					
				case 8:
					System.out.println("Digite el hora inicial:");
					int horaI = lector.nextInt();
					System.out.println("Digite el hora final:");
					int horaF = lector.nextInt();
					System.out.println("Digite la zona de origen:");
					int ZonaOrigen = lector.nextInt();
					System.out.println("Digite la zona de destino: ");
					int ZonaDestino = lector.nextInt();
					Cola rspta = modelo.consultarLosViajesEnFranjaHoraria(horaI, horaF, ZonaOrigen, ZonaDestino);
					
					while( rspta.getTail() != null )
					{
						Uber actual = (Uber) rspta.getTail().getElement();
						System.out.println("Tiempo promedio de viaje: " + actual.getMeanTravel_time());
						System.out.println("Desviaci�n estandar: " + actual.getStandardDeviationTravelTime());
						System.out.println("Hora final: " + horaI);
						System.out.println("Tiempo promedio: " + horaF);
						rspta.dequeue();
					}
					
					break;
				case 9:
					System.out.println("Digite la cantidad de viajes a consultar (N)");
					int N2 = lector.nextInt();
					System.out.println("Digite la hora a consultar: ");
					double hora2 = lector.nextDouble();
					Uber[] rspta3 = modelo.darMayoresPromedio(hora2, N2);
					
					for(int i= 0; i < rspta3.length; i++)
					{
						Uber actual = rspta3[i];
						
						System.out.println("Zona de Origen: " + actual.getSourceId());
						System.out.println("Zona de Destino: " + actual.getDstid());
						System.out.println("Tiempo promedio: " + actual.getMeanTravel_time());
						System.out.println("Desviaci�n estandar: " + actual.getHour());
						
					}
					break;
				case 10:
					System.out.println("Digite la zona de origen:");
					double zonaOrigen = lector.nextDouble();
					System.out.println("Digite la zona de destino: ");
					double zonaDestino = lector.nextDouble();
					System.out.println("Hola");
					Cola tabla = modelo.tablaASCII(zonaOrigen, zonaDestino);
					System.out.println("Metodo f");
					System.out.println("Aproximaci�n en minutos de viajes entre zona origen y zona destino.");
					System.out.println("Trimestre "+ tri + " del 2018 detallado por cada hora del d�a ");
					System.out.println("Zona Origen: "+ zonaOrigen);
					System.out.println("Zona Destino: " + zonaDestino);
					System.out.println("Hora|  # de minutos ");
					
					if(tabla.getTail() != null)
					{
						String txt = "";
						int cont = 0;
						while( tabla.getTail() != null )
						{
							Uber actual = (Uber) tabla.getTail().getElement();
							int numMinutos = (int) actual.getMeanTravel_time() / 60;
							if(numMinutos != 0)
							{
								for (int i = 0; i < numMinutos; i++)
								{
									txt += "*";
								}
								System.out.println(cont+ "  | " + txt);
								txt = "";
							}
							else
							{
								System.out.println(cont + "  |  hora sin viajes" );
							}
							
							cont++;
							tabla.dequeue();
						}
						
					}
					else
					{
						System.out.println("No se encontraron viajes entre la zona digitada");
					}
					
					
					break;
					

				default: 
					System.out.println("--------- \n Opcion Invalida !! \n---------");
					break;
			}
		}
	} 
	
}
