package model.logic;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;


import model.data_structures.Cola;

import model.data_structures.Node;



/**
 * Definicion del modelo del mundo
 *
 */
public class MVCModelo 
{

	public final static String PRIMER_MES = "./data/bogota-cadastral-2018-1-All-MonthlyAggregate.csv";


	public final static String SEGUNDO_MES = "./data/bogota-cadastral-2018-2-All-MonthlyAggregate.csv";


	public final static String TERCER_MES = "./data/bogota-cadastral-2018-3-All-MonthlyAggregate.csv";


	public final static String CUARTO_MES = "./data/bogota-cadastral-2018-4-All-MonthlyAggregate.csv";


	public final static String PRIMER_HORA = "./data/bogota-cadastral-2018-1-All-HourlyAggregate.csv";


	public final static String SEGUNDO_HORA = "./data/bogota-cadastral-2018-2-All-HourlyAggregate.csv";


	public final static String TERCER_HORA = "./data/bogota-cadastral-2018-3-All-HourlyAggregate.csv";


	public final static String CUARTO_HORA = "./data/bogota-cadastral-2018-4-All-HourlyAggregate.csv";


	public final static String PRIMER_SEMANA = "./data/bogota-cadastral-2018-1-WeeklyAggregate.csv";


	public final static String SEGUNDA_SEMANA = "./data/bogota-cadastral-2018-2-WeeklyAggregate.csv";


	public final static String TERCERA_SEMANA = "./data/bogota-cadastral-2018-3-WeeklyAggregate.csv";


	public final static String CUARTA_SEMANA = "./data/bogota-cadastral-2018-4-All-WeeklyAggregate.csv";




	/**
	 * Atributos del modelo del mundo
	 */


	private Cola  colaMes;

	private Cola colaSemana;

	private  Cola colaHora;
	
	private Cola ayuda;



	public MVCModelo(){
		colaMes = new Cola<Uber>();

		colaSemana = new Cola<Uber>();

		colaHora = new Cola<Uber>();
		
		ayuda =new  Cola<Uber>();
	}

	/**
	 * Carga los archivos
	 * @throws Exception Cuando no se encutra el archivo
	 */



	public void cargar (int pTrimestre) throws Exception
	{
		String ruta = "";

		String ruta2 = "";

		String ruta3 = "";

		if(pTrimestre == 1)
		{
			ruta = PRIMER_MES;
			ruta2 = PRIMER_SEMANA;
			ruta3 = PRIMER_HORA;
		}
		else if(pTrimestre == 2)
		{
			ruta = SEGUNDO_MES;
			ruta2 = SEGUNDA_SEMANA;
			ruta3 = SEGUNDO_HORA;

		}
		if(pTrimestre == 3)
		{
			ruta = TERCER_MES;
			ruta2 = TERCERA_SEMANA;
			ruta3 = TERCER_HORA;
		}
		if(pTrimestre == 4)
		{
			ruta = CUARTO_MES;
			ruta2 = CUARTA_SEMANA;
			ruta3 = CUARTO_HORA;
		}
		BufferedReader br = new BufferedReader (new FileReader (ruta));
		String [] datos = new String[7];
		String Linea = br.readLine();
		Linea = br.readLine();

		BufferedReader br2 = new BufferedReader (new FileReader (ruta2));
		String [] datos2 = new String[7];
		String Linea2 = br2.readLine();
		Linea2 = br2.readLine();

				BufferedReader br3 = new BufferedReader (new FileReader (ruta3));
				String [] datos3 = new String[7];
				String Linea3 = br3.readLine();
				Linea3 = br3.readLine();

		while (Linea != null)
		{	
			datos = Linea.split(",");
			double sId = Double.parseDouble(datos[0].trim());
			double dstId = Double.parseDouble(datos[1].trim());
			double hour = Double.parseDouble(datos[2].trim());
			double mtT = Double.parseDouble(datos[3].trim());
			double sdtT = Double.parseDouble(datos[4].trim());
			double gmtT = Double.parseDouble(datos[5].trim());
			double gsdtT = Double.parseDouble(datos[6].trim());

			Uber nuevo = new Uber (sId, dstId, hour, mtT, sdtT, gmtT, gsdtT);
			colaMes.enqueue(nuevo);

			Linea = br.readLine();

		}

		br.close();

		while(Linea2 != null){

			datos2 = Linea2.split(",");
			double sId2 = Double.parseDouble(datos2[0].trim());
			double dstId2 = Double.parseDouble(datos2[1].trim());
			double hour2 = Double.parseDouble(datos2[2].trim());
			double mtT2 = Double.parseDouble(datos2[3].trim());
			double sdtT2 = Double.parseDouble(datos2[4].trim());
			double gmtT2 = Double.parseDouble(datos2[5].trim());
			double gsdtT2 = Double.parseDouble(datos2[6].trim());

			Uber nuevoWeek = new Uber (sId2, dstId2, hour2, mtT2, sdtT2, gmtT2, gsdtT2);
			colaSemana.enqueue(nuevoWeek);
			Linea2 = br2.readLine();

		}	

		br2.close();

				while(Linea3 != null){
		
					datos3 = Linea3.split(",");
					double sId3 = Double.parseDouble(datos3[0].trim());
					double dstId3 = Double.parseDouble(datos3[1].trim());
					double hour3 = Double.parseDouble(datos3[2].trim());
					double mtT3 = Double.parseDouble(datos3[3].trim());
					double sdtT3 = Double.parseDouble(datos3[4].trim());
					double gmtT3 = Double.parseDouble(datos3[5].trim());
					double gsdtT3 = Double.parseDouble(datos3[6].trim());
		
					Uber nuevoHora = new Uber (sId3, dstId3, hour3, mtT3, sdtT3, gmtT3, gsdtT3);
					colaHora.enqueue(nuevoHora);
		
					Linea3 = br3.readLine();
		
				}
		
				br3.close();

	}

	public Cola buscarClusterMasGrande ()
	{
		Cola resultado = new Cola();
		Cola mejor = new Cola();
		double cont = 0;
		double mayor = 0;
		boolean primero = true;
		double hodActual = -1;

		while( colaMes.getTail() != null ){
			Uber viajeActual = (Uber) colaMes.getTail().getElement();
			if( viajeActual.getHour() > hodActual ){
				hodActual = viajeActual.getHour();
				cont++;
				resultado.enqueue(viajeActual);
				colaMes.dequeue();
			}
			else{
				if( cont > mayor ){
					mayor = cont;
					mejor = new Cola();
					while(resultado.getTail() != null){
						Uber actual = (Uber) resultado.getTail().getElement();
						mejor.enqueue(actual);
						resultado.dequeue();
					}
				}
				cont = 0;
				resultado = new Cola();
				resultado.enqueue(viajeActual);
				hodActual = viajeActual.getHour();
				cont++;
				colaMes.dequeue();
			}
		}

		return mejor;

	}

	public Cola darNViajes(double N, double hour)
	{
		Cola viajes = new Cola();
		int cont = 0;

		while(colaMes.getTail() != null && cont < N ){
			cont++;
			Uber viaje = (Uber) colaMes.getTail().getElement();
			if(viaje.getHour() == hour ){
				viajes.enqueue(viaje);
			}

			colaMes.dequeue();
		}

		return viajes ;
	}

	public int totalViajesMes ()
	{
		return colaMes.size(); 	
	}

	public int totalViajesSemana ()
	{
		return colaSemana.size(); 	
	}

	public int totalViajesHora ()
	{
		return colaHora.size(); 	
	}


	public Cola promedioDeViajePorMes(double pMes, double pZonaO, double pZonaD)
	{

		Cola respuesta = new Cola();


		while(colaMes.getTail() != null ){

			Uber viaje = (Uber) colaMes.getTail().getElement();
			if((viaje.getSourceId()> pZonaO)&&(viaje.getDstid() <pZonaD)&& (viaje.getHour() == pMes))
			{

				respuesta.enqueue(viaje);

			}

			colaMes.dequeue();
		}

		return respuesta;
	}

	public Uber[] darMayoresPromedio(double pMes, int N)
	{
		//		Con los primeros datos dado un N
		//		Uber[] respuesta = new Uber[N];
		//		int cont = 0;
		//		while(colaMes.getTail() != null && cont < N )
		//		{
		//			Uber viaje = (Uber) colaMes.getTail().getElement();
		//			if(viaje.getHour() == pMes)
		//			{ 
		//				respuesta[cont] = viaje;
		//				cont++;
		//			}
		//			colaMes.dequeue();
		//		}
		//		 int j;
		//		 for (int i = 1; i < respuesta.length; i++) {
		//		     Uber a = respuesta[i];
		//		     for (j = i - 1; j >=0 && respuesta[j].compareTo(a) <0; j--){
		//		          respuesta[j + 1] = respuesta[j];
		//		     }
		//		     respuesta[j + 1] = a;
		//		 }
		//		
		//
		//		return respuesta;

		//		Todos los datos dado el mes ordenados
		Cola contenido = new  Cola<Uber>();
		int cont =  0;
		while(colaMes.getTail() != null  )
		{
			Uber viaje = (Uber) colaMes.getTail().getElement();
			if(viaje.getHour() == pMes)
			{ 

				contenido.enqueue(viaje);
			}

			colaMes.dequeue();
		}
		Uber[] respuesta = new Uber[contenido.size()];
		while(contenido.getTail() != null  )
		{
			Uber viaje = (Uber) contenido.getTail().getElement();
			respuesta[cont] =viaje;
			cont ++;

			contenido.dequeue();
		}
		int j;
		for (int i = 1; i < respuesta.length; i++) {
			Uber a = respuesta[i];
			for (j = i - 1; j >=0 && respuesta[j].compareTo(a) < 0; j--){
				respuesta[j + 1] = respuesta[j];
				System.out.println("Funciona");
			}
			respuesta[j + 1] = a;
		}


		return respuesta;
	}

	public Cola compararTiemposPromedioPorViaje(int zonaMayor, int zonaMenor, int zonaX, double pMes)
	{
		Cola cVx = new Cola();

		while(colaMes.getTail() != null)
		{
			Uber viaje = (Uber) colaMes.getTail().getElement();
			
			if(viaje.getHour() == pMes )
			{
				if(viaje.getSourceId()> zonaMenor&&(viaje.getDstid() <zonaX))
				{
					cVx.enqueue(viaje);
				}
				else if(viaje.getSourceId()> zonaX&&viaje.getDstid() <zonaMayor)
				{
					ayuda.enqueue(viaje);
				}
				
			}
			colaMes.dequeue();
		}
		return cVx;

	}
	public Cola darColaAyuda()
	{
		return ayuda;
	}

	public Cola supCompararTiemposPromedioViaje(int zonaMayor, int zonaMenor, int zonaX, double pMes)
	{
		Cola xVc = new Cola();
		while(ayuda.getTail() != null)
		{
			Uber viaje = (Uber) ayuda.getTail().getElement();
			if(viaje.getHour() == pMes && (viaje.getSourceId()> zonaX)&&(viaje.getDstid() <zonaMayor))
			{
				xVc.enqueue(viaje);
			}
			ayuda.dequeue();
		}
		return xVc;
	}

	public Cola promedioDeViajePorDia(double pDia, double pZonaO, double pZonaD)
	{
		Cola respuesta = new Cola();


		while(colaSemana.getTail() != null ){

			Uber viaje = (Uber) colaSemana.getTail().getElement();
			if((viaje.getSourceId()> pZonaO)&&(viaje.getDstid() <pZonaD)&& (viaje.getHour() == pDia))
			{
				respuesta.enqueue(viaje);

			}

			colaSemana.dequeue();
		}

		return respuesta;
	}

	public Uber[]darMayoresPromedioPorDia(double pDia, int N)
	{
		Uber[] respuesta = new Uber[N];
		int cont = 0;
		while(colaSemana.getTail() != null && cont < N )
		{
			Uber viaje = (Uber) colaSemana.getTail().getElement();
			if(viaje.getHour() == pDia)
			{ 
				respuesta[cont] = viaje;
				cont++;
			}
			colaSemana.dequeue();
		}
		int j;
		for (int i = 1; i < respuesta.length; i++) {
			Uber a = respuesta[i];
			for (j = i - 1; j >=0 && respuesta[j].compareTo(a) <0; j--){
				respuesta[j + 1] = respuesta[j];
			}
			respuesta[j + 1] = a;
		}


		return respuesta;
	}

	public Cola compararTiemposPromedioPorViajePorDia(int zonaMayor, int zonaMenor, int zonaX, double pDia)
	{
		Cola cVx = new Cola();

		while(colaSemana.getTail() != null)
		{
			Uber viaje = (Uber) colaSemana.getTail().getElement();
			
			if(viaje.getHour() == pDia )
			{
				if(viaje.getSourceId()> zonaMenor&&(viaje.getDstid() <zonaX))
				{
					cVx.enqueue(viaje);
				}
				else if(viaje.getSourceId()> zonaX&&viaje.getDstid() <zonaMayor)
				{
					ayuda.enqueue(viaje);
				}
				
			}
			colaSemana.dequeue();
		}
		return cVx;
	}

	public Cola consultarLosViajesEnFranjaHoraria( int pHoraI,int pHoraF, double zonaO, double zonaD)
	{
		Cola respuesta = new Cola();

		while(colaHora.getTail() != null ){

			Uber viaje = (Uber) colaHora.getTail().getElement();
			if((viaje.getHour()> pHoraI)&&(viaje.getHour() < pHoraF)&& (viaje.getSourceId()> zonaO)&&(viaje.getDstid() < zonaD))
			{
				respuesta.enqueue(viaje);
			}
		}
		return respuesta;

	}

	public Uber[] darMayorPromedioPorHora(int hora, int N)
	{
		Uber[] respuesta = new Uber[N];
		int cont = 0;
		while(colaHora.getTail() != null && cont < N )
		{
			Uber viaje = (Uber) colaHora.getTail().getElement();
			if(viaje.getHour() == hora)
			{ 
				respuesta[cont] = viaje;
				cont++;
			}
		}
		int j;
		for (int i = 1; i < respuesta.length; i++) {
			Uber a = respuesta[i];
			for (j = i - 1; j >=0 && respuesta[j].compareTo(a) <0; j--){
				respuesta[j + 1] = respuesta[j];
			}
			respuesta[j + 1] = a;
		}


		return respuesta;
	}

	public Cola tablaASCII(double zonaO, double zonaD)
	{
		Cola respuesta = new Cola();


		while(colaHora.getTail() != null ){

			Uber viaje = (Uber) colaHora.getTail().getElement();
			if((viaje.getSourceId()> zonaO)&&(viaje.getDstid() <zonaD))
			{
				respuesta.enqueue(viaje);

			}

			colaHora.dequeue();
		}

		return respuesta;

	}


}
